<?php

namespace ReefTests\unit;

use Dompdf\Dompdf;
use PHPUnit\Framework\TestCase;

final class DompdfTest extends TestCase {
	
	private static $Reef;
	private static $Form;
	
	public function testCanSetup(): void {
		
		$Setup = new \Reef\ReefSetup(
			new \Reef\Storage\NoStorageFactory(),
			new \ReefExtra\DomPDF\DomPDF([
				'col' => 0.4,
			]),
			new \Reef\Session\TmpSession()
		);
		
		$s_dir = '/tmp/'.\Reef\unique_id();
		
		static::$Reef = new \Reef\Reef(
			$Setup,
			[
				'cache_dir' => $s_dir,
				'locales' => ['en_US'],
			]
		);
		
		static::$Form = static::$Reef->newTempForm();
		
		static::$Form->newCreator()
			->addField('reef:text_line')
				->setName('a')
				->setLocale([
					'title' => 'Field a',
				])
			->addField('reef:text_number')
				->setName('b')
				->setLocale([
					'title' => 'Field b',
				])
			->addField('reef:checkbox')
				->setName('c')
				->setLocale([
					'title' => 'Field c',
					'box_label' => 'Check?',
				])
			->addField('reef:radio')
				->setName('d')
				->setLocale([
					'title' => 'Field d',
				])
				->set('options', [
					[
						'name' => 'x',
						'locale' => ['en_US' => 'Option x'],
					],
					[
						'name' => 'y',
						'locale' => ['en_US' => 'Option y'],
					],
					[
						'name' => 'z',
						'locale' => ['en_US' => 'Option z'],
					],
				])
			->addField('reef:select')
				->setName('e')
				->setLocale([
					'title' => 'Field e',
				])
				->set('options', [
					[
						'name' => 'x',
						'locale' => ['en_US' => 'Item x'],
					],
					[
						'name' => 'y',
						'locale' => ['en_US' => 'Item y'],
					],
					[
						'name' => 'z',
						'locale' => ['en_US' => 'Item z'],
					],
				])
			->apply();
		
		$Layout = static::$Reef->getSetup()->getLayout();
		$this->assertInstanceof(\ReefExtra\DomPDF\DomPDF::class, $Layout);
		$this->assertSame(0.4, $Layout->getConfig()['col']);
	}
	
	/**
	 * @depends testCanSetup
	 */
	public function testCanGeneratePDF(): void {
		$Submission = static::$Form->newSubmission();
		$Submission->fromStructured([
			'a' => 'Value a',
			'b' => '123',
			'c' => true,
			'd' => 'y',
			'e' => 'z',
		]);
		
		$s_html = static::$Form->generateSubmissionHtml($Submission);
		
		$dompdf = new Dompdf();
		
		$dompdf->loadHtml('<!DOCTYPE html><html><head><title>PDF</title></head><body>'.$s_html.'</body></html>');
		
		$dompdf->setPaper('A4', 'portrait');
		
		$dompdf->render();
		
		$PDF = $dompdf->output();
		
		$this->assertTrue($PDF !== null && strlen($PDF) > 20);
	}
	
}
