<?php

namespace ReefExtra\DomPDF;

use \Reef\Layout\Layout;

/**
 * DomPDF layout
 */
class DomPDF extends Layout {
	
	const COMPONENTS = [
		'reef:upload',
		'reef:text_line',
		'reef:checklist',
		'reef:textarea',
		'reef:select',
		'reef:paragraph',
		'reef:text_number',
		'reef:heading',
		'reef:condition',
		'reef:radio',
		'reef:hidden',
		'reef:option_list',
		'reef:checkbox',
		'reef:submit',
		'reef-extra:likert',
	];
	
	const EXTENSIONS = [
		'reef-extra:label-tooltips',
	];
	
	private $a_config;
	
	public function __construct($a_config = []) {
		$this->a_config = array_merge([
			'col' => 0.3,
		], $a_config);
	}
	
	/**
	 * @inherit
	 */
	public function init($ReefSetup) {
		foreach(static::COMPONENTS as $s_componentName) {
			if($ReefSetup->hasComponent($s_componentName)) {
				$s_subDir = str_replace(':', '/', $s_componentName);
				$ReefSetup->getComponent($s_componentName)->addLayout('dompdf', static::getDir(), 'view/'.$s_subDir);
			}
		}
		
		foreach(static::EXTENSIONS as $s_extensionName) {
			if($ReefSetup->hasExtension($s_extensionName)) {
				$s_subDir = str_replace(':', '/', $s_extensionName);
				$ReefSetup->getExtension($s_extensionName)->addLayout('dompdf', static::getDir(), 'view/'.$s_subDir);
			}
		}
	}
	
	/**
	 * @inherit
	 */
	public static function getName() : string {
		return 'dompdf';
	}
	
	/**
	 * @inherit
	 */
	public static function getDir() : string {
		return __DIR__.'/';
	}
	
	/**
	 * @inherit
	 */
	public function getConfig() : array {
		return $this->a_config;
	}
	
	/**
	 * @inherit
	 */
	public function view(array $a_config = []) : array {
		$a_config = array_merge($this->a_config, $a_config);
		
		return [
			'col_label_perc' => ((float)$a_config['col'])*100,
			'col_value_perc' => (1-(float)$a_config['col'])*100,
		];
	}
	
	/**
	 * @inherit
	 */
	public function getCSS() : array {
		return [
			[
				'type' => 'local',
				'path' => 'submission.css',
				'view' => 'submission',
			]
		];
	}
	
}
